package com.Stage.models;

public class RegistrationDTO {
    private String username;
    private String password;
    private String societe;
    private String email;

    public RegistrationDTO(){
        super();
    }

    public RegistrationDTO(String username,String email,String societe, String password){
        super();
        this.username = username;
        this.email = email;
        this.societe = societe;
        this.password = password;
    }

    public String getUsername(){
        return this.username;
    }

    public void setUsername(String username){
        this.username = username;
    }

    public String getEmail(){return this.email;}
    public void setEmail(String email){ this.email = email;}
    public String getSociete(){
        return this.societe;
    }

    public void setSociete(String societe){
        this.societe = societe;
    }

    public String getPassword(){
        return this.password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String toString(){
        return "Registration info: username: " + this.username +" Email: "+ this.email +" nom de la societe: "+this.societe +" password: " + this.password;
    }
}
