package com.example.spring_stage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringStageApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringStageApplication.class, args);
    }

}
